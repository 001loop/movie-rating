package com.mudrichenko.evgeniy.movierating.core.presentation.theme

import androidx.compose.material.Colors
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.model.ExtendedColors
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.model.Theme

val LocalMaterialColors = staticCompositionLocalOf {
    Colors(
        primary = Color.Unspecified,
        primaryVariant = Color.Unspecified,
        secondary = Color.Unspecified,
        secondaryVariant = Color.Unspecified,
        background = Color.Unspecified,
        surface = Color.Unspecified,
        error = Color.Unspecified,
        onPrimary = Color.Unspecified,
        onSecondary = Color.Unspecified,
        onBackground = Color.Unspecified,
        onSurface = Color.Unspecified,
        onError = Color.Unspecified,
        isLight = true
    )
}

val LocalExtendedColors = staticCompositionLocalOf {
    ExtendedColors(
        extendedColorTest1 = Color.Unspecified
    )
}

/*                  LIGHT                    */
val materialColorsLight = Colors(
    primary = Color(0xFF4B6B67),
    primaryVariant = Color(0xFFA8EFF0),
    secondary = Color(0xFF839290),
    secondaryVariant = Color(0xFFA8EFF0),
    background = Color(0xFFFFFFFF),
    surface = Color(0xFFA8EFF0),
    error = Color(0xFFFF2222),
    onPrimary = Color(0xFFA8EFF0),
    onSecondary = Color(0xFFA8EFF0),
    onBackground = Color(0xFFD6D6D6),
    onSurface = Color(0xFF000000),
    onError = Color(0xFFA8EFF0),
    isLight = true
)

val extendedColorsLight = ExtendedColors(
    extendedColorTest1 = Color(0xFFA8EFF0)
)

/*                  DARK                    */
val materialColorsDark = Colors(
    primary = Color(0xFF4B6B67),
    primaryVariant = Color(0xFFA8EFF0),
    secondary = Color(0xFF839290),
    secondaryVariant = Color(0xFFA8EFF0),
    background = Color(0xFFFFFFFF),
    surface = Color(0xFF3F3F3F),
    error = Color(0xFFA8EFF0),
    onPrimary = Color(0xFFA8EFF0),
    onSecondary = Color(0xFFA8EFF0),
    onBackground = Color(0xFF000000),
    onSurface = Color(0xFFFFFFFF),
    onError = Color(0xFFA8EFF0),
    isLight = true
)

val extendedColorsDark = ExtendedColors(
    extendedColorTest1 = Color(0xFF3F3F3F)
)

/*                  EXTRA ORANGE                    */
val materialColorsExtraOrange = Colors(
    primary = Color(0xFF4B6B67),
    primaryVariant = Color(0xFFA8EFF0),
    secondary = Color(0xFFB6772F),
    secondaryVariant = Color(0xFFA8EFF0),
    background = Color(0xFFFFFFFF),
    surface = Color(0xFFFF9626),
    error = Color(0xFFA8EFF0),
    onPrimary = Color(0xFFA8EFF0),
    onSecondary = Color(0xFFA8EFF0),
    onBackground = Color(0xFF000000),
    onSurface = Color(0xFF050505),
    onError = Color(0xFFA8EFF0),
    isLight = true
)

val extendedColorsExtraOrange = ExtendedColors(
    extendedColorTest1 = Color(0xFFFF9626)
)

@Composable
fun AppTheme(
    theme: Theme,
    isSystemDarkTheme: Boolean,
    content: @Composable () -> Unit
) {
    val materialColors = when (theme) {
        Theme.AUTO -> {
            if (isSystemDarkTheme) materialColorsDark else materialColorsLight
        }
        Theme.LIGHT -> materialColorsLight
        Theme.DARK -> materialColorsDark
        Theme.EXTRA_ORANGE -> materialColorsExtraOrange
    }
    val extendedColors = when (theme) {
        Theme.AUTO -> {
            if (isSystemDarkTheme) extendedColorsDark else extendedColorsLight
        }
        Theme.LIGHT -> extendedColorsLight
        Theme.DARK -> extendedColorsDark
        Theme.EXTRA_ORANGE -> extendedColorsExtraOrange
    }
    CompositionLocalProvider(
        LocalMaterialColors provides materialColors,
        LocalExtendedColors provides extendedColors
    ) {
        MaterialTheme(
            colors = materialColors,
            content = content
        )
    }
}

object AppTheme {
    val colors: Colors
        @Composable
        get() = LocalMaterialColors.current

    val extendedColors: ExtendedColors
        @Composable
        get() = LocalExtendedColors.current
}
