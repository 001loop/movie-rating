package com.mudrichenko.evgeniy.movierating.movie.domain.movie_use_case

interface MovieUseCase {

    suspend fun getHomePageItems()      // todo UseCaseResponse<List<Movie>>

}