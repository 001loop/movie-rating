package com.mudrichenko.evgeniy.movierating.core.presentation.theme.model

enum class Theme(val key: String) {
    AUTO("auto"),
    LIGHT("light"),
    DARK("dark"),
    EXTRA_ORANGE("extra_orange");

    companion object {
        fun getByKey(key: String?): Theme {
            return values().firstOrNull { it.key == key } ?: AUTO
        }
    }
}