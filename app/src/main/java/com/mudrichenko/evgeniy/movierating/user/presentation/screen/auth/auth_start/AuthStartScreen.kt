package com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_start

import android.util.Log
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.AppTheme
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_register.AuthRegisterScreenRoute

object AuthStartScreenRoute {
    const val KEY = "auth_start_screen"
}

@Composable
fun AuthStartScreen(
    mainScreenNavController: NavHostController,
    authScreenNavController: NavHostController
) {

    Log.d("EXCEPTION_BUG", "AuthStartScreen compose")
    BackHandler(true) {
        Log.d("EXCEPTION_BUG", "AuthStartScreen backButton")
//        authScreenNavController.popBackStack()
        mainScreenNavController.popBackStack()
    }

    ConstraintLayout(
        modifier = Modifier
            .background(AppTheme.colors.background)
            .imePadding()       // todo ok
            .safeContentPadding()
//            .wrapContentHeight()
            .fillMaxSize()
//            .fillMaxHeight(0.5f)
    ) {
        val(logoImage, loginButton, registerButton) = createRefs()
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_foreground),
            contentDescription = stringResource(id = R.string.description_logo),
            modifier = Modifier
                .constrainAs(logoImage) {

                    top.linkTo(
                        anchor = parent.top
                    )
//                linkTo(
//
//                )
                }
//                .height(900.dp)

        )
        Button(
            onClick = {
                // todo open another screen.. recompose bottom sheet ??
                authScreenNavController.navigate(AuthRegisterScreenRoute.KEY)
            },
            modifier = Modifier
                .constrainAs(loginButton) {
                    top.linkTo(logoImage.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .padding(top = dimensionResource(id = R.dimen.screenPadding))
        ) {
            Text(text = stringResource(id = R.string.have_account))
        }
        Button(
            onClick = {
                // todo open another screen.. recompose bottom sheet ??
                authScreenNavController.navigate(AuthRegisterScreenRoute.KEY)
            },
            modifier = Modifier
                .constrainAs(registerButton) {
                    top.linkTo(loginButton.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .padding(top = dimensionResource(id = R.dimen.screenPadding))
        ) {
            Text(text = stringResource(id = R.string.create_account))
        }
    }
}