package com.mudrichenko.evgeniy.movierating.movie.presentation.screen.movie_detail

import androidx.lifecycle.ViewModel
import com.mudrichenko.evgeniy.movierating.movie.domain.movie_use_case.MovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val movieUseCase: MovieUseCase
): ViewModel() {

    init {
        loadData()
    }

    private fun loadData() {
        // todo
    }

}