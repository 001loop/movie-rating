package com.mudrichenko.evgeniy.movierating.user.data.model.`object`

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserModel(
    val id: String?,
    val name: String?
): Parcelable {

    fun displayedName(): String {
        return name ?: ""
    }

}