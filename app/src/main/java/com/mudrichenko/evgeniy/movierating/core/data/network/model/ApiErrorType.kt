package com.mudrichenko.evgeniy.movierating.core.data.network.model

enum class ApiErrorType(val key: String) {
    TOKEN_EXPIRED("token_expired"),
    NO_REFRESH_TOKEN("no_refresh_token"),
    INVALID_API_SECRET_KEY("invalid_resource_owner_credentials"),
    NO_USER("no_user"),
    NO_ACCESS("no_access"),
    UNKNOWN("unknown");

    companion object {
        fun getByKey(key: String?): ApiErrorType {
            return values().firstOrNull { it.key == key } ?: UNKNOWN
        }
    }
}