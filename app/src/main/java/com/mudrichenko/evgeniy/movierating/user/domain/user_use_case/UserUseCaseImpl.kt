package com.mudrichenko.evgeniy.movierating.user.domain.user_use_case

import android.util.Log
import com.mudrichenko.evgeniy.movierating.core.data.Mock
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApi
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApiMock
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkResponse
import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.UserModel
import com.mudrichenko.evgeniy.movierating.user.data.model.body.UserBody
import com.mudrichenko.evgeniy.movierating.core.data.storage.Storage
import com.mudrichenko.evgeniy.movierating.user.data.converter.user.UserConverter
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import java.util.*

class UserUseCaseImpl(
    val storage: Storage,
    val mainApi: MainApi,
    val mainApiMock: MainApiMock
): UserUseCase {

    private var userFlow = MutableStateFlow<UserModel?>(null)
    override fun getUserFlow(): MutableStateFlow<UserModel?> {
        return userFlow
    }

    override suspend fun syncUser() {
        val userResponse = getUserRemoteMock()
        when {
            userResponse.data != null -> {
                osUserReceived(userResponse.data)
                Log.d("NETWORK_LOGS", "SUCCESS: data = ${userResponse.data.id}")
            }
            else -> {
                Log.d("NETWORK_LOGS", "ERROR: type = ${userResponse.error?.type} " +
                        "| response = ${userResponse.error?.errorResponseBody}")
                userResponse.error      // todo not null
            }
        }
    }

    private suspend fun osUserReceived(userBody: UserBody) {
        UserConverter.bodyToObject(userBody).let { userModel ->
            saveUserLocal(userModel)
            userFlow.emit(userModel)
        }
    }

    override suspend fun getUser(): UserModel? {
        return getUserLocal()
    }

    private suspend fun getUserLocal(): UserModel? {
        return storage.userModel()
    }

    private suspend fun saveUserLocal(userModel: UserModel) {
        storage.userModel(userModel)
    }

    private suspend fun getUserRemote(): NetworkResponse<UserBody> {
        return mainApiMock.getUser()
    }

    @Mock
    private suspend fun getUserRemoteMock(): NetworkResponse<UserBody> {
        return mainApiMock.getUser()
    }

    override suspend fun authenticationRequired(): Boolean {
        return false        // todo test authentication
    }

    override suspend fun login(email: String, password: String) {
        delay(2000)
        osUserReceived(
            UserBody().apply {
                id = UUID.randomUUID().toString()
                name = "Test User Name"
            }
        )
    }

    override suspend fun logout() {
        storage.userModel(null)
        userFlow.emit(null)
    }

}