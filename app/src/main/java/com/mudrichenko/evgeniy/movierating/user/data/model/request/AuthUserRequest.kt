package com.mudrichenko.evgeniy.movierating.user.data.model.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AuthUserRequest {

    @Expose
    @SerializedName(value = "email")
    var email: String? = null

    @Expose
    @SerializedName(value = "password")
    var password: String? = null

}