package com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button

interface ProgressButtonListener {
    fun onClicked()
}