package com.mudrichenko.evgeniy.movierating.user.presentation.screen.user_data

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController

object UserDataScreenRoute {
    const val KEY = "user_data_screen"
}

@Composable
fun UserDataScreen(
    navHostController: NavHostController
) {
    ConstraintLayout(
        modifier = Modifier
            .background(Color.Green)
            .fillMaxSize(),

        ) {
        val(text, buttonBack, buttonUserDataScreen) = createRefs()
        Text(
            text = "User Data Screen",
            modifier = Modifier.constrainAs(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
//                height = Dimension.fillToConstraints
            }
        )

        Button(
            onClick = {
                navHostController.popBackStack()
            },
            modifier = Modifier.constrainAs(buttonBack) {
                top.linkTo(text.bottom)
                bottom.linkTo(buttonUserDataScreen.top)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            Text(
                text = "Back"
            )
        }

        Button(
            onClick = {
                navHostController.navigate(UserDataScreenRoute.KEY)
            },
            modifier = Modifier.constrainAs(buttonUserDataScreen) {
                top.linkTo(buttonBack.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            Text(
                text = "Open another user data screen"
            )
        }

    }
}