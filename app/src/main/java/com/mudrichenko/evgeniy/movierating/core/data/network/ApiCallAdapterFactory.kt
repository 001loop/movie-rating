package com.mudrichenko.evgeniy.movierating.core.data.network

import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkResponse
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class ApiCallAdapterFactory: CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (getRawType(returnType) != Call::class.java) return null

        check(returnType is ParameterizedType) { "Return type must be a parameterized type." }
        val responseType = getParameterUpperBound(0, returnType)
        if (getRawType(responseType) != NetworkResponse::class.java) return null

        check(responseType is ParameterizedType) { "Response type must be a parameterized type." }
        val successBodyType = getParameterUpperBound(0, responseType)
        return ApiCallAdapter<Any>(successBodyType)
    }

}