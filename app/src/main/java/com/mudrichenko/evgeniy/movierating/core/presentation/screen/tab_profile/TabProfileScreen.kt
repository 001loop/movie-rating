package com.mudrichenko.evgeniy.movierating.core.presentation.screen.tab_profile

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main.ProfileMainScreen
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main.ProfileMainScreenRoute
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main.ProfileMainViewModel
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.user_data.UserDataScreen
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.user_data.UserDataScreenRoute

object TabProfileScreenRoute {
    const val KEY = "tab_profile_screen"
}

@Composable
fun TabProfileScreen(
    mainNavHostController: NavHostController
) {
    val profileNavController = rememberNavController()
    Scaffold { padding ->
        Box(
            modifier = Modifier.padding(bottom = padding.calculateBottomPadding())
        ) {
            ProfileNavigationGraph(
                navHostController = profileNavController,
                mainNavHostController = mainNavHostController
            )
        }
    }
}

@Composable
fun ProfileNavigationGraph(
    navHostController: NavHostController,
    mainNavHostController: NavHostController
) {
    NavHost(
        navController = navHostController,
        startDestination = ProfileMainScreenRoute.KEY
    ) {
        composable(ProfileMainScreenRoute.KEY) {
            ProfileMainScreenDestination(
                navHostController = navHostController,
                mainNavHostController = mainNavHostController
            )
        }
        composable(UserDataScreenRoute.KEY) {
            UserDataScreenDestination(navHostController)
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
private fun ProfileMainScreenDestination(
    navHostController: NavHostController,
    mainNavHostController: NavHostController
) {
    val viewModel: ProfileMainViewModel = hiltViewModel()
    ProfileMainScreen(
        state = viewModel.state,
        mainNavHostController = mainNavHostController,
        navHostController = navHostController
    )
}

@Composable
private fun UserDataScreenDestination(
    navHostController: NavHostController
) {
    UserDataScreen(
        navHostController = navHostController
    )
}