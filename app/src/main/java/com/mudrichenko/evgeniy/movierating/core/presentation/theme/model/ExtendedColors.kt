package com.mudrichenko.evgeniy.movierating.core.presentation.theme.model

import androidx.compose.runtime.Immutable
import androidx.compose.ui.graphics.Color

@Immutable
data class ExtendedColors(
    val extendedColorTest1: Color
)
