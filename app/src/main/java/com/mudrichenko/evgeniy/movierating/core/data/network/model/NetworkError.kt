package com.mudrichenko.evgeniy.movierating.core.data.network.model

class NetworkError(
    val type: NetworkErrorType,
    val errorResponseBody: ErrorResponseBody? = null
)