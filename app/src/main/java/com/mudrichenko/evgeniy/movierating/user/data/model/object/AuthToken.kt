package com.mudrichenko.evgeniy.movierating.user.data.model.`object`

data class AuthToken(
    val accessToken: String?,
    val expiresIn: Long?,
    val refreshToken: String?,
    val tokenType: String?
)