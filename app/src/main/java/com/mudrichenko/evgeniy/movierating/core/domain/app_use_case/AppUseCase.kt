package com.mudrichenko.evgeniy.movierating.core.domain.app_use_case

import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.DataResponse
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.model.Theme
import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.AuthToken

interface AppUseCase {
    suspend fun synchronization(): DataResponse<Unit?>

    fun saveSettingsTheme(theme: Theme?)
    fun getSettingsTheme(): Theme

}