package com.mudrichenko.evgeniy.movierating.user.data.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AuthorizationBody {

    @Expose
    @SerializedName(value = "token")
    var token: AuthTokenBody? = null

    @Expose
    @SerializedName(value = "user")
    var user: UserBody? = null

    fun isValid(): Boolean {
        return token?.isValid() == true
                && user?.isValid() == true
    }

}