package com.mudrichenko.evgeniy.movierating.core.data.network

import com.google.gson.Gson
import com.mudrichenko.evgeniy.movierating.core.data.network.model.ErrorResponseBody
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkError
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkErrorType
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkResponse
import okhttp3.Request
import okhttp3.ResponseBody
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.UnsupportedOperationException
import java.lang.reflect.Type
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class NetworkCall<T>(
    private val call: Call<T>,
    private val successType: Type
): Call<NetworkResponse<T>> {

    override fun enqueue(callback: Callback<NetworkResponse<T>>) = call.enqueue(
        object : Callback<T> {

            override fun onFailure(call: Call<T>, t: Throwable) {
                t.printStackTrace()
                callback.onResponse(
                    this@NetworkCall,
                    Response.success(NetworkResponse(error = t.toNetworkError()))
                )
            }

            override fun onResponse(call: Call<T>, response: Response<T>) {
                callback.onResponse(
                    this@NetworkCall,
                    Response.success(response.toNetworkResponse())
                )
            }

            private fun Throwable.toNetworkError(): NetworkError {
                return when (this) {
                    is UnknownHostException -> NetworkError(NetworkErrorType.NETWORK_DISABLED)
                    is SocketTimeoutException -> NetworkError(NetworkErrorType.TIMEOUT)
                    is ConnectException -> NetworkError(NetworkErrorType.NETWORK_DISABLED)
                    // todo
                    else -> NetworkError(NetworkErrorType.UNKNOWN)
                }
            }

            private fun Response<T>.toNetworkResponse(): NetworkResponse<T> {
                if (!this.isSuccessful) {
                    val errorResponseBody = parseErrorResponseBody(this.errorBody())
                    return if (errorResponseBody != null) {
                        NetworkResponse(
                            error = NetworkError(
                                type = NetworkErrorType.API,
                                errorResponseBody = errorResponseBody
                            )
                        )
                    } else {
                        NetworkResponse(
                            error = NetworkError(
                                type = NetworkErrorType.UNKNOWN
                            )
                        )
                    }
                } else {
                    val body = this.body()
                    return if (body != null) {
                        NetworkResponse(
                            data = body
                        )
                    } else {
                        NetworkResponse(
                            error = NetworkError(
                                type = NetworkErrorType.UNKNOWN
                            )
                        )
                    }
                }
            }

            private fun parseErrorResponseBody(body: ResponseBody?): ErrorResponseBody? {
                return try {
                    Gson().fromJson(body?.string(), ErrorResponseBody::class.java)
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }
            }

        }
    )

    override fun request(): Request {
        return call.request()
    }

    override fun timeout(): Timeout {
        return call.timeout()
    }

    override fun clone(): Call<NetworkResponse<T>> {
        return NetworkCall(call.clone(), successType)
    }

    override fun execute(): Response<NetworkResponse<T>> {
        throw UnsupportedOperationException("NetworkCall doesn't support execute")
    }

    override fun isExecuted(): Boolean {
        return call.isExecuted
    }

    override fun cancel() {
        call.cancel()
    }

    override fun isCanceled(): Boolean {
        return call.isCanceled
    }

}