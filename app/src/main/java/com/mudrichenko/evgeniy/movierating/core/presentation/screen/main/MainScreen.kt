package com.mudrichenko.evgeniy.movierating.core.presentation.screen.main

import android.util.Log
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navDeepLink
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.tab_home.TabHomeScreen
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.tab_home.TabHomeScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.tab_profile.TabProfileScreen
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.tab_profile.TabProfileScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.AppTheme

object MainScreenRoute {
    const val KEY = "main_screen"
}

@OptIn(ExperimentalMaterialNavigationApi::class)
@Composable
fun MainScreen(mainScreenNavController: NavHostController) {
    val navController = rememberNavController()

    Log.d("EXCEPTION_BUG", "MainScreen init compose ${mainScreenNavController.currentBackStackEntry?.destination?.route}")
    val context = LocalContext.current
//    val deepLinkProcessingViewModel = viewModel<MainScreenViewModel>()

    LaunchedEffect(Unit) {
        Log.d("EXCEPTION_BUG", "MainScreen LaunchedEffect")
//        val deepLink = deepLinkProcessingViewModel.processDeepLinkIfAvailable(context)
//        Log.d("EXCEPTION_BUG", "MainScreen $deepLink")

    }
    
    Scaffold(
        modifier = Modifier
            .background(AppTheme.colors.surface)
            .padding(WindowInsets.navigationBars.asPaddingValues()),
        bottomBar = {
            MainBottomNavigation(
                navController = navController
            )
        }
    ) {
        it.calculateBottomPadding()     // todo padding not used! probably we need to set it to the bottom sheet
        MainNavigationGraph(
            mainNavHostController = mainScreenNavController,
            navHostController = navController
        )
    }
}

@Composable
fun MainBottomNavigation(
    navController: NavController
) {
    Log.d("DEEP_LINK_LOGS", "MainBottomNavigation init")
    BottomNavigation(
        backgroundColor = AppTheme.colors.surface,
        contentColor = AppTheme.colors.onSurface
//        modifier = Modifier.padding(WindowInsets.systemBars.asPaddingValues())
//        modifier = Modifier.navigationBarsPadding()     // todo fix inset bottom padding
    ) {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentRoute = navBackStackEntry?.destination?.route ?: BottomNavigationTab.HOME.routeKey
        val bottomNavTabs = remember { BottomNavigationTab.values() }
        bottomNavTabs.forEach { bottomNavigationTab ->
            BottomNavigationItem(
                icon = { Icon(
                    painter = painterResource(bottomNavigationTab.icon),
                    contentDescription = null
                ) },
                label = { Text(
                    text = stringResource(bottomNavigationTab.text)
                )},
                selected = bottomNavigationTab.routeKey == currentRoute,
                onClick = {
                    if (currentRoute != bottomNavigationTab.routeKey) {
                        // баг если быстро переключаться между вкладками. мб из за анимации
                        navController.navigate(bottomNavigationTab.routeKey) {
                            navController.graph.startDestinationRoute?.let { route ->
                                popUpTo(route) {
                                    saveState = true
                                }
                            }
                            launchSingleTop = true
                            restoreState = true
                        }
                    } else {
                        // todo go to top of stack ?
                    }
                }
            )
        }
    }
}

@OptIn(
    ExperimentalMaterialNavigationApi::class,
    ExperimentalMaterialApi::class
)
@Composable
fun MainNavigationGraph(
    mainNavHostController: NavHostController,
    navHostController: NavHostController
) {
//    val mainScreenUriPattern = "${stringResource(id = R.string.deep_link_scheme)}://" +
//            "${stringResource(id = R.string.deep_link_host)}/" +
//            "${MainScreenRoute.KEY}/${TabProfileScreenRoute.KEY}"
    val mainScreenUriPattern = "${stringResource(id = R.string.deep_link_scheme)}://" +
            "${stringResource(id = R.string.deep_link_host)}/" +
            "${MainScreenRoute.KEY}/" +
            "${TabProfileScreenRoute.KEY}"

    NavHost(
        navController = navHostController,
        startDestination = TabHomeScreenRoute.KEY
    ) {
        composable(
            route = TabHomeScreenRoute.KEY
        ) {
            TabHomeScreen(navHostController)
        }
        composable(
            route = TabProfileScreenRoute.KEY,
            deepLinks = listOf(
                navDeepLink { uriPattern = mainScreenUriPattern }
            )
        ) {
            TabProfileScreen(
                mainNavHostController = mainNavHostController
            )
        }
    }
}

enum class BottomNavigationTab(
    @StringRes val text: Int,
    @DrawableRes val icon: Int,
    val routeKey: String
) {
    HOME(R.string.home, R.drawable.ic_mock, TabHomeScreenRoute.KEY),
    PROFILE(R.string.profile, R.drawable.ic_mock, TabProfileScreenRoute.KEY);
}