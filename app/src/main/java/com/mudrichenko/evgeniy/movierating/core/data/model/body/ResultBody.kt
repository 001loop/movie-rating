package com.mudrichenko.evgeniy.movierating.core.data.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResultBody {

    @Expose
    @SerializedName("result")
    var result: String? = null

}