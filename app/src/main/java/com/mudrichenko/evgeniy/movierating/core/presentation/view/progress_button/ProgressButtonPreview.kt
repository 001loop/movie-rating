package com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider


class ProgressButtonModelProvider: PreviewParameterProvider<ProgressButtonModel> {
    override val values = sequenceOf(
        ProgressButtonModel(
            text = "test text",
            isProgress = false
        ),
        ProgressButtonModel(
            text = "",
            isProgress = true
        )
    )
}

@Preview
@Composable
fun PreviewProgressButton(
    @PreviewParameter(ProgressButtonModelProvider::class) model: ProgressButtonModel
) {
    Box(
        modifier = Modifier
            .systemBarsPadding(),
        contentAlignment = Alignment.Center
    ) {
        ProgressButton(
            model = model,
            listener = null
        )
    }
}