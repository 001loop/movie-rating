package com.mudrichenko.evgeniy.movierating.core.data.network.model

import okhttp3.Interceptor
import okhttp3.Request

data class RequestData(
    val chain: Interceptor.Chain,
    val request: Request
)