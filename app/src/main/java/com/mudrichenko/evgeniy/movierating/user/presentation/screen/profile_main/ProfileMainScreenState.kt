package com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main

import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.UserModel

class ProfileMainScreenState(
    val user: UserModel? = null
)