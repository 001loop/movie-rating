package com.mudrichenko.evgeniy.movierating.core.data.network.api

import com.mudrichenko.evgeniy.movierating.BuildConfig
import com.mudrichenko.evgeniy.movierating.core.data.model.body.AppConfigBody
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkResponse
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthTokenBody
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthorizationBody
import com.mudrichenko.evgeniy.movierating.user.data.model.body.UserBody
import com.mudrichenko.evgeniy.movierating.user.data.model.request.AuthUserRequest
import com.mudrichenko.evgeniy.movierating.user.data.model.request.RegisterUserRequest
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface MainApi {

    companion object {
        const val URL = "${BuildConfig.APP_API_HOST}/api/v1/mobile/"
        const val REFRESH_TOKEN_URL = "${URL}v1/mobile/user/auth/token/refresh/"

        const val SECRET_KEY = "secret-key"
        const val AUTHORIZATION = "Authorization"
        const val REFRESH_TOKEN = "refresh-token"
    }

    /* ============ USER ============ */
    /**
     * refresh token on app start if user was logged in.
     */
    @GET("token/refresh")
    fun authTokenRefresh(
        @Header(MainApiMock.REFRESH_TOKEN) refreshToken: String?,
    ): NetworkResponse<AuthTokenBody>

    @POST("user/auth")
    suspend fun authUserMock(
        @Body request: AuthUserRequest
    ): NetworkResponse<AuthorizationBody>

    @POST("user/register")
    suspend fun registerUserMock(
        @Body request: RegisterUserRequest
    ): NetworkResponse<AuthorizationBody>

    @Multipart
    @PUT("user")
    fun updateUserData(
        @Part("name") name: RequestBody?,
        @Part("birthday") birthday: RequestBody?,
        @Part("avatarUrl") avatarUrl: RequestBody?,
        @Part photo: MultipartBody.Part?
    ): NetworkResponse<UserBody>

    /**
     * refresh user data on app start
     */
    @GET("user")
    suspend fun getUser(): NetworkResponse<UserBody>

    /* ============ APP ============ */

    @GET("appConfig")
    suspend fun getAppConfig(
        @HeaderMap timeoutHeaders: Map<String, String> = emptyMap()
    ): NetworkResponse<AppConfigBody>

}