package com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.extensions.parse
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.authentication.AuthenticationScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.main.MainScreenRoute
import kotlinx.coroutines.flow.Flow

object SplashScreenRoute {
    const val KEY = "splash_screen"
}

@ExperimentalComposeUiApi
@Composable
fun SplashScreen(
    state: Flow<SplashScreenState>,
    navHostController: NavHostController
) {
//    val scaffoldState: ScaffoldState = rememberScaffoldState()
//
//    LaunchedEffect(state) {
//        state.collect { state ->
//            if (state.error != null) {
//                Log.d("SYNC_LOGS", "error!")
//                scaffoldState.snackbarHostState.showSnackbar(
//                    message = "sosi hui",
//                    duration = SnackbarDuration.Short
//                )
//            }
//        }
//    }
//    Scaffold(
//        scaffoldState = scaffoldState,
//        modifier = Modifier.navigationBarsPadding()
//    ) {
//        MainLayout(syncProgress = 0, navHostController = navHostController)
//    }

    val screenState by state.collectAsState(SplashScreenState())
    MainLayout(screenState, navHostController)
}

@ExperimentalComposeUiApi
@Composable
fun MainLayout(
    screenState: SplashScreenState,
    navHostController: NavHostController
) {
    Log.d("SYNC_LOGS", "collect | syncProgress: ${screenState.syncProgress}")
    ConstraintLayout(
        modifier = Modifier
            .background(Color.Magenta)
            .fillMaxSize()
            .navigationBarsPadding()
    ) {
        val(logoImage, errorContainer, linearProgress) = createRefs()
        Image(
            painter = painterResource(id = R.drawable.ic_launcher_foreground),
            contentDescription = stringResource(id = R.string.description_logo),
            modifier = Modifier
                .constrainAs(logoImage) {
                    top.linkTo(anchor = parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )
        screenState.endSyncState?.error?.let { appError ->
            ConstraintLayout(
                modifier = Modifier.constrainAs(errorContainer) {
                    bottom.linkTo(linearProgress.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
            ) {
                val(errorText, errorDescription, errorIcon) = createRefs()
                Text(
                    text = appError.parse(),
                    modifier = Modifier.constrainAs(errorText) {
                        top.linkTo(parent.top)
                        bottom.linkTo(errorDescription.top)
                        start.linkTo(parent.start)
                        end.linkTo(errorIcon.start)
                    }
                )
                Text(
                    text = stringResource(id = R.string.cant_start_app),
                    modifier = Modifier.constrainAs(errorDescription) {
                        top.linkTo(errorText.bottom)
                        bottom.linkTo(parent.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(errorIcon.start)
                    }
                )
                Image(
                    painter = painterResource(id = R.drawable.ic_launcher_foreground),
                    contentDescription = stringResource(id = R.string.description_logo),
                    modifier = Modifier
                        .constrainAs(errorIcon) {
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                            start.linkTo(errorText.end)
                            end.linkTo(parent.end)
                        }
                )
            }
        }
        LinearProgressIndicator(
            progress = (screenState.syncProgress) / 100f,
            modifier = Modifier
                .constrainAs(linearProgress) {
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .fillMaxWidth()
        )
    }

    screenState.endSyncState?.authenticationRequired?.let { authenticationRequired ->
        if (screenState.endSyncState.error != null) {
            return
        }
        when (authenticationRequired) {
            true -> {
                LaunchedEffect(Unit) {
                    openAuthenticationScreen(navHostController)
                }
            }
            false -> {
                LaunchedEffect(Unit) {
                    openMainScreen(navHostController)
                }
            }
        }
    }
}

private fun openMainScreen(navHostController: NavHostController) {
    navHostController.navigate(MainScreenRoute.KEY) {
        popUpTo(SplashScreenRoute.KEY) {
            inclusive = true
        }
    }
}

private fun openAuthenticationScreen(navHostController: NavHostController) {
    navHostController.navigate(AuthenticationScreenRoute.KEY) {
        popUpTo(SplashScreenRoute.KEY) {
            inclusive = true
        }
    }
}