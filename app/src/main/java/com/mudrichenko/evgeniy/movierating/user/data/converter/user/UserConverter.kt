package com.mudrichenko.evgeniy.movierating.user.data.converter.user

import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.UserModel
import com.mudrichenko.evgeniy.movierating.user.data.model.body.UserBody

object UserConverter {

    fun bodyToObject(body: UserBody): UserModel {
        return UserModel(
            id = body.id,
            name = body.name
        )
    }

}