package com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_register

import android.util.Log
import android.util.Patterns
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.onFocusedBoundsChanged
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.AppTheme
import com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button.ProgressButton
import com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button.ProgressButtonListener
import com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button.ProgressButtonModel
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main.ProfileMainScreenState
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main.ProfileMainViewModel

object AuthRegisterScreenRoute {
    const val KEY = "auth_register_screen"
}

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
fun AuthRegisterScreen(
    mainScreenNavController: NavHostController,
    authScreenNavController: NavHostController,
    modalBottomSheetState: ModalBottomSheetState
) {
    val viewModel: AuthRegisterScreenViewModel = hiltViewModel()
    val screenState by viewModel.state.collectAsState(initial = AuthRegisterScreenState())
    BackHandler(true) {
        authScreenNavController.popBackStack()
    }
    val focusManager = LocalFocusManager.current
    val focusRequesterEmail = remember { FocusRequester() }
    val focusRequesterPassword = remember { FocusRequester() }

    var email by remember { mutableStateOf("") }
    var emailError: String? by remember { mutableStateOf(null) }
    var password by remember { mutableStateOf("") }
    var passwordError: String? by remember { mutableStateOf(null) }

    ConstraintLayout(
        modifier = Modifier
            .imePadding()       // todo ok
            .background(AppTheme.colors.background)
            .safeContentPadding()
            .padding(dimensionResource(id = R.dimen.screenPadding))
            .fillMaxSize()
    ) {
        val(inputFieldsLayout, titleText, emailTextField, passwordTextField, nextButton) = createRefs()

        LaunchedEffect(Unit) {
            Log.d("BOTTOM_LOGS", "request focus!")
            if (modalBottomSheetState.isVisible) {
                focusRequesterEmail.requestFocus()
            }
        }

        Column(
            modifier = Modifier
                .constrainAs(inputFieldsLayout) {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                },
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Text(
                text = stringResource(id = R.string.create_account),
            )

            OutlinedTextField(
                value = email,
                onValueChange = {
                    email = it.trim()
                },
                enabled = !screenState.isLoading,
                label = { Text(stringResource(id = R.string.email)) },
                isError = emailError != null,
                modifier = Modifier
                    .padding(
                        top = dimensionResource(id = R.dimen.screenItemsMarginLarge)
                    )
                    .fillMaxWidth()
                    .focusRequester(focusRequesterEmail),
                keyboardActions = KeyboardActions(
                    onNext = { focusManager.moveFocus(FocusDirection.Down) }
                )
            )

            OutlinedTextField(
                value = password,
                onValueChange = {
                    password = it.trim()
                },
                enabled = !screenState.isLoading,
                label = { Text(stringResource(id = R.string.password)) },
                isError = passwordError != null,
                modifier = Modifier
                    .padding(
                        top = dimensionResource(id = R.dimen.screenItemsMarginDefault)
                    )
                    .fillMaxWidth()
                    .focusRequester(focusRequesterPassword)
            )

            ProgressButton(
                modifier = Modifier
                    .padding(
                        top = dimensionResource(id = R.dimen.screenItemsMarginLarge)
                    )
                    .fillMaxWidth(),
                model = ProgressButtonModel(
                    text = stringResource(id = R.string.next),
                    isProgress = screenState.isLoading
                ),
                listener = object : ProgressButtonListener {
                    override fun onClicked() {
                        emailError = if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                            null
                        } else {
                            "wrong email!"
                        }
                        passwordError = if (password.length > 4) {
                            null
                        } else {
                            "wrong password!"
                        }

                        if (emailError == null && passwordError == null) {
                            // todo progress bar
                            // todo lock input fields and buttons when loading
                            viewModel.login(email, password)
                        }
                    }
                }
            )
        }
    }

}