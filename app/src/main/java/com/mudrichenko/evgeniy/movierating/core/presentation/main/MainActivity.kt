package com.mudrichenko.evgeniy.movierating.core.presentation.main

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.core.view.WindowCompat
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.navDeepLink
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.google.accompanist.navigation.material.*
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.authentication.AuthenticationScreen
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.authentication.AuthenticationScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.main.MainScreen
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.main.MainScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash.SplashScreen
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash.SplashScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash.SplashScreenViewModel
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.AppTheme
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.AuthScreen
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.AuthScreenRoute
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalComposeUiApi
@ExperimentalMaterialNavigationApi
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@AndroidEntryPoint
class MainActivity: ComponentActivity() {

    private val viewModel by viewModels<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, true)

        setContent {
            AppTheme(
                theme = viewModel.getTheme(),
                isSystemDarkTheme = isSystemInDarkTheme()
            ) {
                MainContent()
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

}

@ExperimentalComposeUiApi
@ExperimentalMaterialNavigationApi
@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
private fun MainContent() {
    val modalBottomSheetState = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        skipHalfExpanded = true,
        confirmStateChange = {
            Log.d("BOTTOM_STATE_LOGS", "confirmStateChange = ${it.name}")
//            it != ModalBottomSheetValue.HalfExpanded
            false
            it == ModalBottomSheetValue.Expanded
        }
    )

    val bottomSheetNavigator = remember { BottomSheetNavigator(modalBottomSheetState) }
    val mainScreenNavController = rememberAnimatedNavController(bottomSheetNavigator)

    val mainScreenUriPattern = "${stringResource(id = R.string.deep_link_scheme)}://" +
            "${stringResource(id = R.string.deep_link_host)}/" +
            "${MainScreenRoute.KEY}"

    Log.d("DEEP_LINK_LOGS", "MAIN ACTIVITY: ${mainScreenUriPattern} | ${modalBottomSheetState.isVisible}")


    if (!modalBottomSheetState.isVisible) {
//        mainScreenNavController.popBackStack()
        Log.d("EXCEPTION_BUG", "BOTTOM SHEET HIDE!  ${mainScreenNavController.currentBackStackEntry?.destination?.route}")
        // todo handle error You cannot access the NavBackStackEntry's ViewModels after the NavBackStackEntry is destroyed.
//        mainScreenNavController.popBackStack()
        LocalFocusManager.current.clearFocus()
    }

    ModalBottomSheetLayout(
        bottomSheetNavigator = bottomSheetNavigator,
        modifier = Modifier.fillMaxSize(),
    ) {
        AnimatedNavHost(
            navController = mainScreenNavController,
            startDestination = SplashScreenRoute.KEY,
//            contentAlignment = Alignment.BottomCenter
        ) {
            composable(
                route = SplashScreenRoute.KEY,
//            deepLinks = listOf(
//                navDeepLink {
//                    uriPattern = mainScreenUriPattern
//                }
//            )
            ) {
                SplashScreenDestination(mainScreenNavController)
            }
            composable(
                route = MainScreenRoute.KEY,
                deepLinks = listOf(
                    navDeepLink { uriPattern = mainScreenUriPattern }
                )
            ) {
                MainScreenDestination(mainScreenNavController)
            }
            composable(
                route = AuthenticationScreenRoute.KEY
            ) {
                AuthenticationScreenDestination(mainScreenNavController)
            }
//            bottomSheet(
//                route = AuthStartScreenRoute.KEY
//            ) {
//                AuthStartScreen(mainScreenNavController)    // todo is it ok
//            }
//            bottomSheet(
//                route = AuthRegisterScreenRoute.KEY
//            ) {
//                AuthRegisterScreen(mainScreenNavController, modalBottomSheetState)    // todo is it ok
//            }
            bottomSheet(
                route = AuthScreenRoute.KEY
            ) {
                AuthScreen(mainScreenNavController, modalBottomSheetState)    // todo is it ok
            }
        }
    }
}

@ExperimentalComposeUiApi
@Composable
private fun SplashScreenDestination(navHostController: NavHostController) {
    val viewModel: SplashScreenViewModel = hiltViewModel()
    SplashScreen(
        state = viewModel.state,
        navHostController = navHostController
    )
}

@Composable
private fun MainScreenDestination(mainScreenNavController: NavHostController) {
    MainScreen(mainScreenNavController)
}

@Composable
private fun AuthenticationScreenDestination(navHostController: NavHostController) {
    AuthenticationScreen(
        navHostController = navHostController
    )
}