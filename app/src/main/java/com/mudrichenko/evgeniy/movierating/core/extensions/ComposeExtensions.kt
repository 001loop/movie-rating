package com.mudrichenko.evgeniy.movierating.core.extensions

import androidx.compose.runtime.*
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.res.pluralStringResource
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.AppError

@Composable
fun Lifecycle.observeAsSate(): State<Lifecycle.Event> {
    val state = remember { mutableStateOf(Lifecycle.Event.ON_ANY) }
    DisposableEffect(this) {
        val observer = LifecycleEventObserver { _, event ->
            state.value = event
        }
        this@observeAsSate.addObserver(observer)
        onDispose {
            this@observeAsSate.removeObserver(observer)
        }
    }
    return state
}

@ExperimentalComposeUiApi
@Composable
fun AppError.parse(): String {
    return when {
        this.pluralCount != null && this.formatArgs != null && this.messageStringRes != null -> {
            pluralStringResource(
                id = this.messageStringRes,
                count = this.pluralCount,
                this.formatArgs
            )
        }
        this.pluralCount != null && this.messageStringRes != null -> {
            pluralStringResource(
                id = this.messageStringRes,
                count = this.pluralCount
            )
        }
        this.formatArgs != null && this.messageStringRes != null -> {
            stringResource(
                id = this.messageStringRes,
                this.formatArgs
            )
        }
        this.messageStringRes != null -> {
            stringResource(
                id = this.messageStringRes
            )
        }
        else -> message ?: ""
    }
}

