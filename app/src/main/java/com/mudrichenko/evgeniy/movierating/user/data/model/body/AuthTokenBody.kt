package com.mudrichenko.evgeniy.movierating.user.data.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AuthTokenBody {

    @Expose
    @SerializedName(value = "accessToken")
    var accessToken: String? = null

    @Expose
    @SerializedName(value = "expiresIn")
    var expiresIn: Long? = null

    @Expose
    @SerializedName(value = "refreshToken")
    var refreshToken: String? = null

    @Expose
    @SerializedName(value = "tokenType")
    var tokenType: String? = null

    fun isValid(): Boolean {
        return accessToken != null
                && refreshToken != null
                && expiresIn != null
                && tokenType != null
    }

}