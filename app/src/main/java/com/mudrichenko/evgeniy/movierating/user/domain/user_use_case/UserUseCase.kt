package com.mudrichenko.evgeniy.movierating.user.domain.user_use_case

import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.UserModel
import kotlinx.coroutines.flow.MutableStateFlow

interface UserUseCase {
    fun getUserFlow(): MutableStateFlow<UserModel?>

    suspend fun syncUser()  // call on app start
    suspend fun getUser(): UserModel?
//    suspend fun logout()

    suspend fun authenticationRequired(): Boolean

    suspend fun login(email: String, password: String)
    suspend fun logout()
}