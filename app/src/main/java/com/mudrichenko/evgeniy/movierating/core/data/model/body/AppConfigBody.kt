package com.mudrichenko.evgeniy.movierating.core.data.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AppConfigBody {

    @Expose
    @SerializedName("newUserRegistrationEnabled")
    var newUserRegistrationEnabled: Boolean? = null

}