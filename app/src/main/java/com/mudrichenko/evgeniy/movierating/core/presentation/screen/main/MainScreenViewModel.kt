package com.mudrichenko.evgeniy.movierating.core.presentation.screen.main

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.mudrichenko.evgeniy.movierating.core.extensions.findActivity
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainScreenViewModel @Inject constructor(

): ViewModel() {

    init {


        Log.d("EXCEPTION_BUG", "MainScreenViewModel init")
    }

    private var deepLinkProcessed = false

    fun processDeepLinkIfAvailable(context: Context): String? {
        if (!deepLinkProcessed) {
            val activity = context.findActivity()
            val intentData = activity?.intent?.data?.toString()
            deepLinkProcessed = true
            return intentData
        }
        return null
    }

}