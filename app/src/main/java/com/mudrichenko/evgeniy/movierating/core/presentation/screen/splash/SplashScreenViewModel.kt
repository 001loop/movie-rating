package com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mudrichenko.evgeniy.movierating.core.domain.app_use_case.AppUseCase
import com.mudrichenko.evgeniy.movierating.user.domain.user_use_case.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashScreenViewModel @Inject constructor(
    val appUseCase: AppUseCase,
    val userUseCase: UserUseCase
): ViewModel() {

    var state = MutableStateFlow<SplashScreenState>(SplashScreenState())
        private set

    init {
        syncData()
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("SYNC_LOGS", "Splash VM cleared")
    }

    private fun syncData() {
        viewModelScope.launch {
            syncUser()
            syncAppData()
        }
    }

    private fun syncUser() {
        viewModelScope.launch {
            userUseCase.syncUser()
            state.emit(
                SplashScreenState(
                    syncProgress = 50
                )
            )
        }
    }

    private fun syncAppData() {
        viewModelScope.launch {
            val response = appUseCase.synchronization()
            state.emit(
                SplashScreenState(
                    syncProgress = 100,
                    endSyncState = SplashScreenState.EndSyncState(
                        error = response.appError(),
                        authenticationRequired = userUseCase.authenticationRequired()
                    )
                )
            )
        }
    }

}