package com.mudrichenko.evgeniy.movierating.movie.di.use_case

import com.mudrichenko.evgeniy.movierating.core.di.StorageModule
import com.mudrichenko.evgeniy.movierating.movie.domain.movie_use_case.MovieUseCase
import com.mudrichenko.evgeniy.movierating.movie.domain.movie_use_case.MovieUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [StorageModule::class])
@InstallIn(SingletonComponent::class)
class MovieUseCaseModule {

    @Provides
    @Singleton
    fun provideMovieUseCase(
    ): MovieUseCase {
        return MovieUseCaseImpl(
        )
    }

}