package com.mudrichenko.evgeniy.movierating.user.data.model.body

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserBody {

    @Expose
    @SerializedName("id")
    var id: String? = null

    @Expose
    @SerializedName("name")
    var name: String? = null

    @Expose
    @SerializedName("birthday")
    var birthday: Long? = null

    @Expose
    @SerializedName("avatarUrl")
    var avatarUrl: String? = null

    fun isValid(): Boolean {
        return id?.isNotEmpty() == true
    }

}