package com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button

import android.util.Log.d
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.AppTheme

@Composable
fun ProgressButton(
    modifier: Modifier = Modifier,
    model: ProgressButtonModel,
    listener: ProgressButtonListener?
) {
    Box(
        modifier = modifier
//            .border()
            .padding(dimensionResource(id = R.dimen.buttonPadding))
            .background(
                color = AppTheme.colors.primary,
                shape = RoundedCornerShape(dimensionResource(id = R.dimen.cornerRadiusDefault))
            )
//            .clip(RoundedCornerShape(dimensionResource(id = R.dimen.cornerRadiusDefault)))
//            .border(
//                width = 0.dp,
//                color = AppTheme.colors.primary,
//                shape = RoundedCornerShape(dimensionResource(id = R.dimen.cornerRadiusDefault))
//            )
            .clickable {
                listener?.onClicked()
            }
            .fillMaxWidth()
            .wrapContentHeight(),
        contentAlignment = Alignment.Center
    ) {
//        if (!model.isProgress) {
//            Text(
//                modifier = Modifier
//                    .wrapContentHeight(),
//                text = model.text ?: "",
//            )
//        }
        Text(
            modifier = Modifier
                .wrapContentHeight()
                .alpha(
                    if (model.isProgress)
                        0.0f
                    else
                        1.0f
                ),
            text = model.text ?: "",
        )
        CircularProgressIndicator(
            modifier = Modifier
                .alpha(
                    if (model.isProgress)
                        1.0f
                    else
                        0.0f
                )
                .wrapContentSize(),
//                    .fillMaxHeight()
        color = AppTheme.colors.primaryVariant
        )
//        if (model.isProgress) {
//            CircularProgressIndicator(
//                modifier = Modifier
//                    .alpha(
//                        if (model.isProgress)
//                            0.0f
//                        else
//                            1.0f
//                    ),
////                    .fillMaxHeight()
//            )
//        }
    }
}