package com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button

class ProgressButtonModel(
    var text: String?,
    var isProgress: Boolean
)