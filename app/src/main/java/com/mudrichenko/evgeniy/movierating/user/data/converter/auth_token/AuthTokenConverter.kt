package com.mudrichenko.evgeniy.movierating.user.data.converter.auth_token

import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.AuthToken
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthTokenBody

object AuthTokenConverter {

    fun bodyToObject(body: AuthTokenBody): AuthToken {
        return AuthToken(
            accessToken = body.accessToken,
            expiresIn = body.expiresIn,
            refreshToken = body.refreshToken,
            tokenType = body.tokenType
        )
    }

}