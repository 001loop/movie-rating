package com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash

import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.AppError

class SplashScreenState(
    val syncProgress: Int = 0,
    val endSyncState: EndSyncState? = null
) {

    class EndSyncState(
        val error: AppError? = null,
        val authenticationRequired: Boolean
    )

}