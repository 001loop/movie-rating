package com.mudrichenko.evgeniy.movierating.core.data.network.interceptor

import com.google.gson.Gson
import com.mudrichenko.evgeniy.movierating.BuildConfig
import com.mudrichenko.evgeniy.movierating.core.AppSettings
import com.mudrichenko.evgeniy.movierating.core.data.network.model.RequestData
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApi
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApiMock
import com.mudrichenko.evgeniy.movierating.core.data.network.model.ErrorResponseBody
import com.mudrichenko.evgeniy.movierating.core.data.storage.Storage
import com.mudrichenko.evgeniy.movierating.user.data.converter.auth_token.AuthTokenConverter
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthTokenBody
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit

class HeadersInterceptor(
    private val storage: Storage
): Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestData = handleTimeoutHeaders(chain)
        var response = getResponse(requestData)
        if(response.code == HttpURLConnection.HTTP_UNAUTHORIZED && isNeedToRefreshToken(response)) {
            val wasTokenRefreshed = refreshToken(requestData.chain)
            if (wasTokenRefreshed) {
                response = getResponse(requestData)
            }
        }
        return response
    }

    private fun getResponse(requestData: RequestData): Response {
        return requestData.chain.proceed(addBaseHeaders(requestData.request))
    }

    private fun handleTimeoutHeaders(chain: Interceptor.Chain): RequestData {
        var interceptorChain = chain
        var request = chain.request()
        interceptorChain.request().header(AppSettings.HTTP_HEADER_CONNECT_TIMEOUT)?.toIntOrNull()?.let {
            request = removeHeader(request, AppSettings.HTTP_HEADER_CONNECT_TIMEOUT)
            interceptorChain = interceptorChain.withConnectTimeout(it, TimeUnit.MILLISECONDS)
        }
        interceptorChain.request().header(AppSettings.HTTP_HEADER_READ_TIMEOUT)?.toIntOrNull()?.let {
            request = removeHeader(request, AppSettings.HTTP_HEADER_READ_TIMEOUT)
            interceptorChain = interceptorChain.withReadTimeout(it, TimeUnit.MILLISECONDS)
        }
        interceptorChain.request().header(AppSettings.HTTP_HEADER_WRITE_TIMEOUT)?.toIntOrNull()?.let {
            request = removeHeader(request, AppSettings.HTTP_HEADER_WRITE_TIMEOUT)
            interceptorChain = interceptorChain.withWriteTimeout(it, TimeUnit.MILLISECONDS)
        }
        return RequestData(interceptorChain, request)
    }

    private fun removeHeader(request: Request, headerName: String): Request {
        return request.newBuilder().removeHeader(headerName).build()
    }

    private fun addBaseHeaders(request: Request): Request {
        val builder = request.newBuilder()

        val token = storage.authToken()
        if (token != null) {
            builder.header(
                if (AppSettings.IS_MOCK_API) MainApiMock.AUTHORIZATION else MainApi.AUTHORIZATION,
                "${token.tokenType} ${token.accessToken}"
            )
        }
        builder.header(
            if (AppSettings.IS_MOCK_API) MainApiMock.SECRET_KEY else MainApi.SECRET_KEY,
            BuildConfig.APP_API_SECRET_KEY
        )

        return builder.build()
    }

    private fun refreshToken(chain: Interceptor.Chain): Boolean {
        val refreshToken = storage.authToken()?.refreshToken
        val request = Request.Builder()
            .url(MainApi.REFRESH_TOKEN_URL)
            .get()
            .header(
                if (AppSettings.IS_MOCK_API) MainApiMock.REFRESH_TOKEN else MainApi.REFRESH_TOKEN,
                "$refreshToken"
            )
            .build()

        val response = chain.proceed(request)

        if (response.isSuccessful) {
            val responseJson = response.body?.string()
            try {
                val newToken = Gson().fromJson(
                    responseJson,
                    AuthTokenBody::class.java
                )
                if(newToken.isValid()) {
                    storage.authToken(
                        AuthTokenConverter.bodyToObject(newToken)
                    )
                    return true
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return false
    }

    private fun isNeedToRefreshToken(response: Response): Boolean {
        var errorBody: ErrorResponseBody? = null
        try {
            errorBody = Gson().fromJson(
                response.peekBody(Long.MAX_VALUE).string(),
                ErrorResponseBody::class.java
            )
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return errorBody?.isTokenExpired() ?: false
    }

}