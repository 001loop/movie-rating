package com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ModalBottomSheetState
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.AppTheme
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_register.AuthRegisterScreen
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_register.AuthRegisterScreenRoute
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_start.AuthStartScreen
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_start.AuthStartScreenRoute

object AuthScreenRoute {
    const val KEY = "auth_screen"
}

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
fun AuthScreen(
    mainScreenNavController: NavHostController,
    modalBottomSheetState: ModalBottomSheetState
) {
    BackHandler(true) {
        mainScreenNavController.popBackStack()
    }
    val authNavController = rememberNavController()
    Scaffold { padding ->
        Box(
            modifier = Modifier
                .padding(padding)
                .background(AppTheme.colors.background)
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            AuthNavigationGraph(
                authScreenNavController = authNavController,
                mainScreenNavController = mainScreenNavController,
                modalBottomSheetState = modalBottomSheetState
            )
        }
    }
}

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
fun AuthNavigationGraph(
    authScreenNavController: NavHostController,
    mainScreenNavController: NavHostController,
    modalBottomSheetState: ModalBottomSheetState
) {
    NavHost(
        navController = authScreenNavController,
        startDestination = AuthStartScreenRoute.KEY
    ) {
        composable(AuthStartScreenRoute.KEY) {
            AuthStartScreenDestination(
                mainScreenNavController = mainScreenNavController,
                authScreenNavController = authScreenNavController
            )
        }
        composable(AuthRegisterScreenRoute.KEY) {
            AuthRegisterScreenDestination(
                mainScreenNavController = mainScreenNavController,
                authScreenNavController = authScreenNavController,
                modalBottomSheetState = modalBottomSheetState
            )
        }
    }
}

@Composable
private fun AuthStartScreenDestination(
    mainScreenNavController: NavHostController,
    authScreenNavController: NavHostController
) {
    AuthStartScreen(
        mainScreenNavController = mainScreenNavController,
        authScreenNavController = authScreenNavController
    )
}

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
private fun AuthRegisterScreenDestination(
    mainScreenNavController: NavHostController,
    authScreenNavController: NavHostController,
    modalBottomSheetState: ModalBottomSheetState
) {
    AuthRegisterScreen(
        mainScreenNavController = mainScreenNavController,
        authScreenNavController = authScreenNavController,
        modalBottomSheetState = modalBottomSheetState
    )
}