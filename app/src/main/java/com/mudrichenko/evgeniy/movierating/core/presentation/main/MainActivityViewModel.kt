package com.mudrichenko.evgeniy.movierating.core.presentation.main

import androidx.lifecycle.ViewModel
import com.mudrichenko.evgeniy.movierating.core.domain.app_use_case.AppUseCase
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.model.Theme
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val appUseCase: AppUseCase
): ViewModel() {

    fun getTheme(): Theme {
        return appUseCase.getSettingsTheme()
    }

}