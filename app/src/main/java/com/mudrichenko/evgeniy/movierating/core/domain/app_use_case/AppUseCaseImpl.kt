package com.mudrichenko.evgeniy.movierating.core.domain.app_use_case

import android.util.Log
import com.mudrichenko.evgeniy.movierating.core.AppSettings
import com.mudrichenko.evgeniy.movierating.core.data.converter.AppConfigConverter
import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.AppConfig
import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.AppError
import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.DataResponse
import com.mudrichenko.evgeniy.movierating.core.data.model.body.AppConfigBody
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApi
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApiMock
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkResponse
import com.mudrichenko.evgeniy.movierating.core.data.storage.Storage
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.model.Theme
import com.mudrichenko.evgeniy.movierating.core.util.CoreUtil
import kotlinx.coroutines.delay

class AppUseCaseImpl(
    val storage: Storage,
    val mainApi: MainApi,
    val mainApiMock: MainApiMock
): AppUseCase {

    /**
     * Get from server all necessary data for the application to work
     * We can`t let user enter the application if there is no appConfig data (in that case we just
     * show error on splash screen)
     */
    override suspend fun synchronization(): DataResponse<Unit?> {
        saveAppConfigLocal(null)        // todo remove. that for testing
        getAppConfigRemote().let { networkResponse ->
            Log.d("RESPONSE_LOGS", "data = ${networkResponse.data} | error = ${networkResponse.error?.type}")
            // save app config into local storage if response was successful
            networkResponse.data?.let { appConfigBody ->
                Log.d("RESPONSE_LOGS", "appConfigBody received! newUserRegistrationEnabled = ${appConfigBody.newUserRegistrationEnabled}")
                saveAppConfigLocal(AppConfigConverter.bodyToObject(appConfigBody))
            }
            // get app config from local storage.
            // return response error if there is no app config in local storage
            if (getAppConfigLocal() != null) {
                Log.d("RESPONSE_LOGS", "DataResponse.Successful")
                return DataResponse.Successful(Unit)
            } else {
                Log.d("RESPONSE_LOGS", "DataResponse.Error")
                return DataResponse.Error(AppError(networkResponse.error))
            }
        }
    }

    private suspend fun getAppConfigRemote(): NetworkResponse<AppConfigBody> {
        return if (AppSettings.IS_MOCK_API)
            getAppConfigRemoteMock()
        else
            mainApi.getAppConfig(
                timeoutHeaders = CoreUtil.getTimeoutHeaders(AppSettings.CONNECTION_TIMEOUT_MILLIS_SHORT)
            )
    }

    private suspend fun getAppConfigRemoteMock(): NetworkResponse<AppConfigBody> {
        delay(AppSettings.MOCK_API_RESPONSE_DELAY)
        return mainApiMock.getAppConfig(
            timeoutHeaders = CoreUtil.getTimeoutHeaders(AppSettings.CONNECTION_TIMEOUT_MILLIS_SHORT)
        )
    }

    private fun getAppConfigLocal(): AppConfig? {
        return storage.appConfig()
    }

    private fun saveAppConfigLocal(appConfig: AppConfig?) {
        storage.appConfig(appConfig)
    }

    /**
     * Theme of application. Can be changed by user on settings screen
     */
    override fun saveSettingsTheme(theme: Theme?) {
        storage.settingsTheme(theme ?: Theme.AUTO)
    }

    override fun getSettingsTheme(): Theme {
        return storage.settingsTheme()
    }

}