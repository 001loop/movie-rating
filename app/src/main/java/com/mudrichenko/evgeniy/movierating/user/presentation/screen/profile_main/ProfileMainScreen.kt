package com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.UserModel
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.AuthScreenRoute
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_register.AuthRegisterScreenRoute
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow

object ProfileMainScreenRoute {
    const val KEY = "profile_main_screen"
}

lateinit var viewModel: ProfileMainViewModel

@ExperimentalMaterialApi
@Composable
fun ProfileMainScreen(
    state: Flow<ProfileMainScreenState>,
    mainNavHostController: NavHostController,
    navHostController: NavHostController,
) {
    viewModel = viewModel<ProfileMainViewModel>()
    val screenState by state.collectAsState(initial = ProfileMainScreenState())
    screenState.user.let { user ->
        when(user) {
            null -> UserNotExistScreen(mainNavHostController)
            else -> UserExistScreen(user)
        }
    }
}

@Composable
fun UserExistScreen(
    user: UserModel
) {
    ConstraintLayout(
        modifier = Modifier
            .background(Color.Magenta)
            .fillMaxSize()
    ) {
        val(text, button) = createRefs()
        Text(
            text = user.displayedName(),
            modifier = Modifier.constrainAs(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
//                height = Dimension.fillToConstraints
            }
        )

        Button(
            onClick = {
                viewModel.logout()
            },
            modifier = Modifier.constrainAs(button) {
                top.linkTo(text.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            Text(
                text = "Logout"
            )
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun UserNotExistScreen(
    mainNavHostController: NavHostController
) {
    UserNotExistView(
        mainNavHostController = mainNavHostController
    )
}

@ExperimentalMaterialApi
@Composable
private fun UserNotExistView(
    mainNavHostController: NavHostController
) {
    ConstraintLayout(
        modifier = Modifier
            .background(Color.Red)
            .fillMaxSize()
    ) {
        val (text, button) = createRefs()
        Text(
            text = "Profile Screen",
            modifier = Modifier.constrainAs(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        )
        Button(
            onClick = {
                mainNavHostController.navigate(AuthScreenRoute.KEY)
            },
            modifier = Modifier.constrainAs(button) {
                top.linkTo(text.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
            }
        ) {
            Text(
                text = "Login"
            )
        }
    }
}
