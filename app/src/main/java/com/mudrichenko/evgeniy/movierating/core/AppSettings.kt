package com.mudrichenko.evgeniy.movierating.core

class AppSettings {

    companion object {

        // ===================================        BASE      ==================================
        /**
         * 4-digits pass-code protection on app start (if user was logged in)
         */
        const val IS_AUTHENTICATION_REQUIRED = true

        /**
         * Mock api with fake responses
         */
        const val IS_MOCK_API = true
        const val MOCK_API_RESPONSE_DELAY = 1000L

        // ===================================        HTTP      ==================================
        /**
         * Default timeout: .core.di.NetworkModule.CONNECTION_TIMEOUT_DURATION
         */
        const val CONNECTION_TIMEOUT_MILLIS_SHORT = 5000
        const val HTTP_HEADER_CONNECT_TIMEOUT = "CONNECT_TIMEOUT"
        const val HTTP_HEADER_READ_TIMEOUT = "READ_TIMEOUT"
        const val HTTP_HEADER_WRITE_TIMEOUT = "WRITE_TIMEOUT"

    }

}