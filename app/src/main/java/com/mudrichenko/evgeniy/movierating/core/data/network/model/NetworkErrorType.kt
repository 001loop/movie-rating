package com.mudrichenko.evgeniy.movierating.core.data.network.model

enum class NetworkErrorType {
    NETWORK_DISABLED, TIMEOUT, API, UNKNOWN;
}