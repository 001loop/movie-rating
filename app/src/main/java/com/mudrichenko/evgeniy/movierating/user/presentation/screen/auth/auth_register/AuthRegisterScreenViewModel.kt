package com.mudrichenko.evgeniy.movierating.user.presentation.screen.auth.auth_register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mudrichenko.evgeniy.movierating.user.domain.user_use_case.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthRegisterScreenViewModel @Inject constructor(
    private val userUseCase: UserUseCase
): ViewModel() {

    var state = MutableStateFlow(AuthRegisterScreenState())
        private set

    fun login(email: String, password: String) {
        viewModelScope.launch {
            state.emit(
                AuthRegisterScreenState(
                    isLoading = true
                )
            )
            userUseCase.login(email, password)
            state.emit(
                AuthRegisterScreenState(
                    isLoading = false
                )
            )
        }
    }

}