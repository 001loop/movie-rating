package com.mudrichenko.evgeniy.movierating.core.data.model.`object`

import androidx.annotation.StringRes
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkError

class AppError {

    @StringRes val messageStringRes: Int?
    val formatArgs: Array<Any?>?
    val pluralCount: Int?

    val message: String?

    constructor(
        messageStringRes: Int? = null,
        formatArgs: Array<Any?>? = null,
        pluralCount: Int? = null,
        message: String? = null
    ) {
        this.messageStringRes = messageStringRes
        this.formatArgs = formatArgs
        this.pluralCount = pluralCount
        this.message = message
    }

    constructor(networkError: NetworkError?) {
        // todo parse network error
        this.messageStringRes = R.string.error_unknown
        this.formatArgs = null
        this.pluralCount = null
        this.message = null
    }

}