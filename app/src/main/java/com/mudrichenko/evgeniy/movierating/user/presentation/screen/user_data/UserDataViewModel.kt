package com.mudrichenko.evgeniy.movierating.user.presentation.screen.user_data

import androidx.lifecycle.ViewModel
import com.mudrichenko.evgeniy.movierating.user.domain.user_use_case.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UserDataViewModel @Inject constructor(
    private val userUseCase: UserUseCase
): ViewModel() {

    init {
        loadData()
    }

    private fun loadData() {
        // todo
    }

}