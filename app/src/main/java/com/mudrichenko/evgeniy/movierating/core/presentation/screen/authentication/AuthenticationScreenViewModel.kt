package com.mudrichenko.evgeniy.movierating.core.presentation.screen.authentication

import androidx.lifecycle.ViewModel
import com.mudrichenko.evgeniy.movierating.user.domain.user_use_case.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AuthenticationScreenViewModel @Inject constructor(
    val userUseCase: UserUseCase
): ViewModel() {

    // todo show pass code screen
    // todo 1) pass code entered correctly: close that screen, go to mainScreen
    // todo 2) pass code entered wrong 5 times (also store wrongCount in preferences): logout and then go to mainScreen. P.s. Show info about remaining tries
    // todo 3) clicked on button "logout": logout and then go to mainScreen

}