package com.mudrichenko.evgeniy.movierating.core.util

import com.mudrichenko.evgeniy.movierating.core.AppSettings

object CoreUtil {

    fun getTimeoutHeaders(timeout: Int): Map<String, String> {
        return mapOf(
            Pair(AppSettings.HTTP_HEADER_CONNECT_TIMEOUT, "$timeout"),
            Pair(AppSettings.HTTP_HEADER_WRITE_TIMEOUT, "$timeout"),
            Pair(AppSettings.HTTP_HEADER_READ_TIMEOUT, "$timeout")
        )
    }

}