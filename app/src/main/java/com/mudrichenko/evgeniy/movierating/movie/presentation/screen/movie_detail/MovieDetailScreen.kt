package com.mudrichenko.evgeniy.movierating.movie.presentation.screen.movie_detail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.navigation.NavHostController

object MovieDetailScreenRoute {
    const val KEY = "movie_detail_screen"
}

@Composable
fun MovieDetailScreen(
    navHostController: NavHostController
) {
    ConstraintLayout(
        modifier = Modifier
            .background(Color.Yellow)
            .fillMaxSize(),

        ) {
        val(text, button) = createRefs()
        Text(
            text = "Movie id:",     // todo get id from arguments
            modifier = Modifier.constrainAs(text) {
                top.linkTo(parent.top)
                bottom.linkTo(parent.bottom)
                start.linkTo(parent.start)
                end.linkTo(parent.end)
//                height = Dimension.fillToConstraints
            }
        )
    }
}