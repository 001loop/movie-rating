package com.mudrichenko.evgeniy.movierating.core.data.network.api

import com.google.gson.Gson
import com.mudrichenko.evgeniy.movierating.core.data.model.body.AppConfigBody
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthTokenBody
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthorizationBody
import com.mudrichenko.evgeniy.movierating.user.data.model.body.UserBody

class MainApiMockResponses {

    private val authTokenBody = AuthTokenBody().apply {
        this.accessToken = "RANDOM_ACCESS_TOKEN"
        this.expiresIn = 10000L
        this.refreshToken = "RANDOM_REFRESH_TOKEN"
        this.tokenType = "Bearer"
    }

    fun getAuthToken(): String {
        return Gson().toJson(authTokenBody).toString()
    }

    private val userBody = UserBody().apply {
        this.id = "RANDOM_USER_ID"
        this.name = "Evgeniy Mudrichenko"
        this.birthday = 763084800000L
        this.avatarUrl = "https://picsum.photos/200"
    }

    fun getUser(): String {
        return Gson().toJson(userBody).toString()
    }

    private val authorization = AuthorizationBody().apply {
        this.token = authTokenBody
        this.user = userBody
    }

    fun getAuthorization(): String {
        return Gson().toJson(authorization).toString()
    }

    private val appConfigBody = AppConfigBody().apply {
        this.newUserRegistrationEnabled = true
    }

    fun getAppConfig(): String {
        return Gson().toJson(appConfigBody).toString()
    }

}