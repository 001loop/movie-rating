package com.mudrichenko.evgeniy.movierating.user.di.use_case

import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApi
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApiMock
import com.mudrichenko.evgeniy.movierating.core.data.storage.Storage
import com.mudrichenko.evgeniy.movierating.core.di.NetworkModule
import com.mudrichenko.evgeniy.movierating.core.di.StorageModule
import com.mudrichenko.evgeniy.movierating.user.domain.user_use_case.UserUseCase
import com.mudrichenko.evgeniy.movierating.user.domain.user_use_case.UserUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [StorageModule::class, NetworkModule::class])
@InstallIn(SingletonComponent::class)
class UserUseCaseModule {

    @Provides
    @Singleton
    fun provideUserUseCase(
        storage: Storage,
        mainApi: MainApi,
        mainApiMock: MainApiMock
    ): UserUseCase {
        return UserUseCaseImpl(
            storage = storage,
            mainApi = mainApi,
            mainApiMock = mainApiMock
        )
    }

}