package com.mudrichenko.evgeniy.movierating.core.di.use_case

import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApi
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApiMock
import com.mudrichenko.evgeniy.movierating.core.data.storage.Storage
import com.mudrichenko.evgeniy.movierating.core.di.NetworkModule
import com.mudrichenko.evgeniy.movierating.core.di.StorageModule
import com.mudrichenko.evgeniy.movierating.core.domain.app_use_case.AppUseCase
import com.mudrichenko.evgeniy.movierating.core.domain.app_use_case.AppUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module(includes = [StorageModule::class, NetworkModule::class])
@InstallIn(SingletonComponent::class)
class AppUseCaseModule {

    @Provides
    @Singleton
    fun provideAppUseCase(
        storage: Storage,
        mainApi: MainApi,
        mainApiMock: MainApiMock
    ): AppUseCase {
        return AppUseCaseImpl(
            storage = storage,
            mainApi = mainApi,
            mainApiMock = mainApiMock
        )
    }

}