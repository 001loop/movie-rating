package com.mudrichenko.evgeniy.movierating.core.di

import co.infinum.retromock.Retromock
import com.google.gson.Gson
import com.mudrichenko.evgeniy.movierating.core.data.network.ApiCallAdapterFactory
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApi
import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApiMock
import com.mudrichenko.evgeniy.movierating.core.data.network.interceptor.HeadersInterceptor
import com.mudrichenko.evgeniy.movierating.core.data.storage.Storage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    companion object {
        const val CONNECTION_TIMEOUT_DURATION = 30L
        val CONNECTION_TIMEOUT_UNIT = TimeUnit.SECONDS
    }

    @Provides
    @Singleton
    fun provideHeadersInterceptor(storage: Storage): HeadersInterceptor {
        return HeadersInterceptor(
            storage = storage
        )
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(headersInterceptor: HeadersInterceptor): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .followRedirects(true)
            .followSslRedirects(true)
            .connectTimeout(CONNECTION_TIMEOUT_DURATION, CONNECTION_TIMEOUT_UNIT)
            .readTimeout(CONNECTION_TIMEOUT_DURATION, CONNECTION_TIMEOUT_UNIT)
            .writeTimeout(CONNECTION_TIMEOUT_DURATION, CONNECTION_TIMEOUT_UNIT)
            .addInterceptor(headersInterceptor)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofitBuilder(gson: Gson): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(ApiCallAdapterFactory())
    }

    @Provides
    @Singleton
    fun provideMockRetrofitBuilder(
        retrofitBuilder: Retrofit.Builder,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return retrofitBuilder
            .baseUrl(MainApi.URL)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideMainApi(retrofit: Retrofit): MainApi {
        return retrofit
            .create(MainApi::class.java)
    }

    @Provides
    @Singleton
    fun provideMainApiMock(retrofit: Retrofit): MainApiMock {
        return Retromock.Builder()
            .retrofit(retrofit)
            .build()
            .create(MainApiMock::class.java)
    }

}