package com.mudrichenko.evgeniy.movierating.core.data.network.model

class NetworkResponse<T> {

    val data: T?

    val error: NetworkError?

    constructor(
        data: T
    ) {
        this.data = data
        this.error = null
    }

    constructor(
        error: NetworkError
    ) {
        this.data = null
        this.error = error
    }

}