package com.mudrichenko.evgeniy.movierating.core.presentation.screen.authentication

import android.widget.Button
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalWindowInfo
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintLayoutScope
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.NavHostController
import com.mudrichenko.evgeniy.movierating.R
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.main.MainScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash.MainLayout
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash.SplashScreenRoute
import com.mudrichenko.evgeniy.movierating.core.presentation.screen.splash.SplashScreenState
import com.mudrichenko.evgeniy.movierating.user.presentation.screen.user_data.UserDataScreenRoute
import kotlinx.coroutines.flow.Flow

object AuthenticationScreenRoute {
    const val KEY = "authentication_screen"
}

@Composable
fun AuthenticationScreen(
    navHostController: NavHostController
) {
//    modifier = Modifier.padding(WindowInsets.systemBars.asPaddingValues())
    ConstraintLayout(
        modifier = Modifier
            .background(Color.Green)
            .fillMaxSize()
            .padding(WindowInsets.systemBars.asPaddingValues())
            .padding(dimensionResource(id = R.dimen.screenPadding))
    ) {

        val(
            button1, button2, button3, button4, button5, button6, button7, button8, button9,
            button0, buttonBackspace, buttonFingerprint, buttonLogout, titleTextView
        ) = createRefs()

        Text(
            text = stringResource(id = R.string.enter_code),
            modifier = Modifier
                .constrainAs(titleTextView) {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
        )

        Button(
            onClick = {
                onNumberClicked(navHostController, 1)
            },
            modifier = Modifier
                .constrainAs(button1) {
                    top.linkTo(titleTextView.bottom)
                    end.linkTo(button2.start)
                }
                .fillMaxWidth(.2f)
                .padding(dimensionResource(id = R.dimen.authenticationScreenButtonPadding))
        ) {
            Text(
                text = "1"
            )
        }

        Button(
            onClick = {
                onNumberClicked(navHostController, 2)
            },
            modifier = Modifier
                .constrainAs(button2) {
                    top.linkTo(titleTextView.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }
                .fillMaxWidth(.2f)
                .padding(dimensionResource(id = R.dimen.authenticationScreenButtonPadding))
        ) {
            Text(
                text = "2"
            )
        }

        Button(
            onClick = {
                onNumberClicked(navHostController, 3)
            },
            modifier = Modifier
                .constrainAs(button3) {
                    top.linkTo(titleTextView.bottom)
                    start.linkTo(button2.end)
                }
                .fillMaxWidth(.2f)
                .padding(dimensionResource(id = R.dimen.authenticationScreenButtonPadding))
        ) {
            Text(
                text = "3"
            )
        }
    }
}

private fun onNumberClicked(navHostController: NavHostController, number: Int) {
    // todo check is max num of chars already entered
    // todo if == max then send data to viewModel. check pass code validity
    if (number == 3) {
        navHostController.navigate(MainScreenRoute.KEY) {
            popUpTo(AuthenticationScreenRoute.KEY) {
                inclusive = true
            }
        }
    }
}
