package com.mudrichenko.evgeniy.movierating.core.presentation.screen.tab_home

import android.util.Log
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.mudrichenko.evgeniy.movierating.movie.presentation.screen.home.HomeScreen
import com.mudrichenko.evgeniy.movierating.movie.presentation.screen.home.HomeScreenRoute

object TabHomeScreenRoute {
    const val KEY = "tab_home_screen"
}

@Composable
fun TabHomeScreen(
    mainNavHostController: NavHostController
) {
    val homeNavController = rememberNavController()

    Scaffold { padding ->
        Box(
            modifier = Modifier.padding(bottom = padding.calculateBottomPadding())
        ) {
            HomeNavigationGraph(
                navHostController = homeNavController,
            )
        }
    }
}

@Composable
fun HomeNavigationGraph(
    navHostController: NavHostController
) {
    NavHost(            // TODO Already attached to lifecycleOwner
        navController = navHostController,
        startDestination = HomeScreenRoute.KEY
    ) {
        composable(HomeScreenRoute.KEY) {
            HomeScreen(navHostController)
        }
    }
}