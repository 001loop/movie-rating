package com.mudrichenko.evgeniy.movierating.user.data.model.request

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegisterUserRequest {

    @Expose
    @SerializedName(value = "email")
    var email: String? = null

    @Expose
    @SerializedName(value = "password")
    var password: String? = null

    @Expose
    @SerializedName(value = "name")
    var name: String? = null

    @Expose
    @SerializedName(value = "birthdate")
    var birthdate: Long? = null

}