package com.mudrichenko.evgeniy.movierating.core.data.network.api

import co.infinum.retromock.meta.Mock
import co.infinum.retromock.meta.MockResponse
import com.mudrichenko.evgeniy.movierating.BuildConfig
import com.mudrichenko.evgeniy.movierating.core.data.model.body.AppConfigBody
import com.mudrichenko.evgeniy.movierating.core.data.network.model.NetworkResponse
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthTokenBody
import com.mudrichenko.evgeniy.movierating.user.data.model.body.AuthorizationBody
import com.mudrichenko.evgeniy.movierating.user.data.model.body.UserBody
import com.mudrichenko.evgeniy.movierating.user.data.model.request.AuthUserRequest
import com.mudrichenko.evgeniy.movierating.user.data.model.request.RegisterUserRequest
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

/**
 * Class for testing api calls without using a real server
 *
 * Body on MockResponse must be a compile-time constant, we can`t directly using constructions like
 * MainApiMockResponses().getAuthTokenRefresh() etc. So, for getting a string for mockResponse
 * body use MainApiMockResponses class and MockResponseTest
 */
interface MainApiMock {

    companion object {
        const val URL = "${BuildConfig.APP_API_HOST}/api/v1/mobile/"
        const val REFRESH_TOKEN_URL = "${URL}v1/mobile/user/auth/token/refresh/"

        const val SECRET_KEY = "secret-key"
        const val AUTHORIZATION = "Authorization"
        const val REFRESH_TOKEN = "refresh-token"

        const val authTokenResponseBody = "{" +
                    "\"accessToken\":\"RANDOM_ACCESS_TOKEN\"," +
                    "\"expiresIn\":10000," +
                    "\"refreshToken\":\"RANDOM_REFRESH_TOKEN\"," +
                    "\"tokenType\":\"Bearer\"" +
                "}"
        const val authorizationResponseBody = "{" +
                    "\"token\":{" +
                        "\"accessToken\":\"RANDOM_ACCESS_TOKEN\"," +
                        "\"expiresIn\":10000," +
                        "\"refreshToken\":\"RANDOM_REFRESH_TOKEN\"," +
                        "\"tokenType\":\"Bearer\"" +
                    "}," +
                    "\"user\":{" +
                        "\"id\":\"RANDOM_USER_ID\"," +
                        "\"name\":\"Evgeniy Mudrichenko\"," +
                        "\"birthday\":763084800000," +
                        "\"avatarUrl\":\"https://picsum.photos/200\"" +
                    "}" +
                "}"
        const val userResponseBody = "{" +
                    "\"id\":\"RANDOM_USER_ID\"," +
                    "\"name\":\"Evgeniy Mudrichenko\"," +
                    "\"birthday\":763084800000," +
                    "\"avatarUrl\":\"https://picsum.photos/200\"" +
                "}"
        const val appConfigResponseBody = "{" +
                    "\"newUserRegistrationEnabled\":true" +
//                "\"sadasd\":true" +
                "}"
    }

    /* ============ USER ============ */
    /**
     * refresh token on app start if user was logged in.
     */
    @Mock
    @MockResponse(body = authTokenResponseBody)
    @GET("token/refresh")
    fun authTokenRefresh(
        @Header(REFRESH_TOKEN) refreshToken: String?,
    ): NetworkResponse<AuthTokenBody>

    @Mock
    @MockResponse(body = authorizationResponseBody)
    @POST("user/auth")
    suspend fun authUserMock(
        @Body request: AuthUserRequest
    ): NetworkResponse<AuthorizationBody>

    @Mock
    @MockResponse(body = authorizationResponseBody)
    @POST("user/register")
    suspend fun registerUserMock(
        @Body request: RegisterUserRequest
    ): NetworkResponse<AuthorizationBody>

    @Mock
    @MockResponse(body = userResponseBody)
    @Multipart
    @PUT("user")
    fun updateUserData(
        @Part("name") name: RequestBody?,
        @Part("birthday") birthday: RequestBody?,
        @Part("avatarUrl") avatarUrl: RequestBody?,
        @Part photo: MultipartBody.Part?
    ): NetworkResponse<UserBody>

    /**
     * refresh user data on app start
     */
    @Mock
    @MockResponse(body = userResponseBody)
    @GET("user")
    suspend fun getUser(): NetworkResponse<UserBody>

    /* ============ APP ============ */

    @Mock
    @MockResponse(body = appConfigResponseBody)
    @GET("appConfig")
    suspend fun getAppConfig(
        @HeaderMap timeoutHeaders: Map<String, String> = emptyMap()
    ): NetworkResponse<AppConfigBody>

}