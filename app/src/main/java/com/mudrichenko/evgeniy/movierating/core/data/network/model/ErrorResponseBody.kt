package com.mudrichenko.evgeniy.movierating.core.data.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ErrorResponseBody {

    @Expose
    @SerializedName(value = "key")
    val key: String? = null

    @Expose
    @SerializedName(value = "data")
    val data: Any? = null         // todo придумай апишку по которой в этом поле будет возвращаться важная инфа которую надо юзеру отобразить
    // например "ваш лимит оценок в день исчерпан. Плоти нологи либо жди еще n времени. в поле message будет приходить тогда Long time когда сбросится лимит

    fun isTokenExpired(): Boolean {
        return key == ApiErrorType.TOKEN_EXPIRED.key
    }

}