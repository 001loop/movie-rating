package com.mudrichenko.evgeniy.movierating.core.data.model.`object`

data class AppConfig(
    val newUserRegistrationEnabled: Boolean?
)