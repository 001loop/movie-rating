package com.mudrichenko.evgeniy.movierating.core.data.storage

import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.AppConfig
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.model.Theme
import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.UserModel
import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.AuthToken

interface Storage {
    fun authToken(): AuthToken?
    fun authToken(authToken: AuthToken?)

    fun userModel(): UserModel?
    fun userModel(userModel: UserModel?)

    fun settingsTheme(): Theme
    fun settingsTheme(theme: Theme)

    fun appConfig(): AppConfig?
    fun appConfig(appConfig: AppConfig?)
}