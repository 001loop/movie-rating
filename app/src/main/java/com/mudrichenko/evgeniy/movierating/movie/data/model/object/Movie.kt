package com.mudrichenko.evgeniy.movierating.movie.data.model.`object`

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Movie(
    var id: String,
    var name: String?
): Parcelable