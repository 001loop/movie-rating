package com.mudrichenko.evgeniy.movierating.user.presentation.screen.profile_main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mudrichenko.evgeniy.movierating.user.domain.user_use_case.UserUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileMainViewModel @Inject constructor(
    private val userUseCase: UserUseCase
): ViewModel() {

    var state = MutableStateFlow<ProfileMainScreenState>(ProfileMainScreenState())
        private set

    init {
        initScreen()
    }

    private fun initScreen() {
        viewModelScope.launch {
            userUseCase.getUserFlow().collectLatest { userModel ->
                state.emit(
                    ProfileMainScreenState(
                        user = userModel
                    )
                )
            }
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch {
            userUseCase.login(email, password)
        }
    }

    fun logout() {
        viewModelScope.launch {
            userUseCase.logout()
        }
    }

}