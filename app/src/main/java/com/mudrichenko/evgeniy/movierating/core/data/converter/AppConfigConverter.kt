package com.mudrichenko.evgeniy.movierating.core.data.converter

import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.AppConfig
import com.mudrichenko.evgeniy.movierating.core.data.model.body.AppConfigBody

object AppConfigConverter {

    fun bodyToObject(body: AppConfigBody): AppConfig {
        return AppConfig(
            newUserRegistrationEnabled = body.newUserRegistrationEnabled
        )
    }

}