package com.mudrichenko.evgeniy.movierating.core.data.storage

import android.content.SharedPreferences
import com.google.gson.Gson
import com.mudrichenko.evgeniy.movierating.core.data.model.`object`.AppConfig
import com.mudrichenko.evgeniy.movierating.core.presentation.theme.model.Theme
import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.UserModel
import com.mudrichenko.evgeniy.movierating.user.data.model.`object`.AuthToken

class StorageImpl(
    private val preferences: SharedPreferences,
    private val gson: Gson
): Storage {

    // ========================== AUTH TOKEN ========================== //
    private val AUTH_TOKEN = "authToken"

    override fun authToken(): AuthToken? {
        return preferences.getString(AUTH_TOKEN, null)?.let {
            gson.fromJson(it, AuthToken::class.java)
        }
    }

    override fun authToken(authToken: AuthToken?) {
        preferences.edit().putString(AUTH_TOKEN, authToken?.let { gson.toJson(it) }).apply()
    }

    // ========================== USER MODEL ========================== //
    private val USER_MODEL = "userModel"

    override fun userModel(): UserModel? {
        return preferences.getString(USER_MODEL, null)?.let {
            gson.fromJson(it, UserModel::class.java)
        }
    }

    override fun userModel(userModel: UserModel?) {
        preferences.edit().putString(USER_MODEL, userModel?.let { gson.toJson(it) }).apply()
    }

    // ========================== SETTINGS THEME ========================== //
    private val SETTINGS_THEME = "settingsTheme"

    override fun settingsTheme(): Theme {
        return Theme.getByKey(
            preferences.getString(SETTINGS_THEME, null)
        )
    }

    override fun settingsTheme(theme: Theme) {
        preferences.edit().putString(SETTINGS_THEME, theme.name).apply()
    }

    // ========================== APP CONFIG ========================== //
    private val APP_CONFIG = "appConfig"

    override fun appConfig(): AppConfig? {
        return preferences.getString(APP_CONFIG, null)?.let {
            gson.fromJson(it, AppConfig::class.java)
        }
    }

    override fun appConfig(appConfig: AppConfig?) {
        preferences.edit().putString(APP_CONFIG, appConfig?.let { gson.toJson(it) }).apply()
    }

}