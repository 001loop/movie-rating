package com.mudrichenko.evgeniy.movierating.view

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.mudrichenko.evgeniy.movierating.core.presentation.main.MainActivity
import com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button.ProgressButton
import com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button.ProgressButtonListener
import com.mudrichenko.evgeniy.movierating.core.presentation.view.progress_button.ProgressButtonModel
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@RunWith(AndroidJUnit4::class)
class ProgressButtonTest {
    @get:Rule
    val composeTestRule = createComposeRule()

    @Test
    fun clickTest() {

        var isClicked = false
        // Start the app
        composeTestRule.setContent {
            MaterialTheme {
                ProgressButton(
                    model = ProgressButtonModel(
                        text = "click me!",
                        isProgress = false
                    ),
                    listener = object : ProgressButtonListener {
                        override fun onClicked() {
                            isClicked = true
                        }
                    }
                )
            }
        }

        composeTestRule.onNodeWithText("click me!").assertIsDisplayed()
        composeTestRule.onNodeWithText("click me!").performClick()
        assertTrue(isClicked)
    }
}