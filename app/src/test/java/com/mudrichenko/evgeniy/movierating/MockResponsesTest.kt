package com.mudrichenko.evgeniy.movierating

import com.mudrichenko.evgeniy.movierating.core.data.network.api.MainApiMockResponses
import org.junit.Test

class MockResponsesTest {

    @Test
    fun getMockResponsesStrings() {
        println("authToken: ${MainApiMockResponses().getAuthToken()}")
        println("user: ${MainApiMockResponses().getUser()}")
        println("authorization: ${MainApiMockResponses().getAuthorization()}")
        println("appConfig: ${MainApiMockResponses().getAppConfig()}")
    }

}